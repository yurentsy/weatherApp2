package com.yurentsy.weatherapp;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String LAST_SELECT_CITY = "last_select_city";
    public final static String PARAM_STATUS = "status";
    public final static String BROADCAST_ACTION = "com.yurentsy.weatherapp.servicebackbroadcast";
    public final static String BROADCAST_WIDGET = "android.appwidget.action.APPWIDGET_UPDATE";
    public final static int STATUS_FINISH = 200;
    private static final String POSITIVE_BUTTON_TEXT = "Go";
    boolean bind = false;
    ServiceConnection sConn;
    WeatherService service;
    private RecyclerView recyclerView;
    private myAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private SharedPreferences sharedPreferences;
    private String selectCity;
    private BroadcastReceiver br;
    private boolean firstReceive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get RecyclerView
        recyclerView = findViewById(R.id.cities_recycler_view);


//        cities = new CityData(getBaseContext());

        //begin set RecyclerView
        //set LayaoutManager
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        //set Adapter
        adapter = new myAdapter();
//        adapter = new myAdapter(cities);
        recyclerView.setAdapter(adapter);
        //end set RecyclerView

        sConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
//                Toast.makeText(getBaseContext(), "onServiceConnected", Toast.LENGTH_SHORT).show();
                service = ((WeatherService.ExampleBinder) binder).getService();
                bind = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
//                Toast.makeText(getBaseContext(), "onServiceDisconnected", Toast.LENGTH_SHORT).show();
                bind = false;
            }
        };
        Intent intent = new Intent(getBaseContext(), WeatherService.class);
        bindService(intent, sConn, BIND_AUTO_CREATE);

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(PARAM_STATUS, 0);
                if (status == STATUS_FINISH) {
                    if (service == null) return;
                    adapter.setData(service.getCities());
                    adapter.notifyDataSetChanged();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(br, intentFilter);

        if (firstReceive) {
            loadSharedPreference(adapter.getCities());
            firstReceive = false;
        }
    }

    public void showPopup(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_add:
                        addElement();
                        break;
                    case R.id.menu_clear:
                        clearList();
                        break;
                    default:
                        return false;
                }
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        popup.inflate(R.menu.main_menu);
        popup.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveSharedPreference();
        if (bind) {
            service.setLastSelect(selectCity);
            unregisterReceiver(br);
            unbindService(sConn);
//            service.onDestroy();
        }
        bind = false;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                addElement();
                break;
//            case R.id.menu_refresh:
//                break;
            case R.id.menu_clear:
                clearList();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void addElement() {
//        cities.appendCity("Moscow");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.input_city_dialog));
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(POSITIVE_BUTTON_TEXT, (dialog, which) -> {
            if (!input.getText().toString().isEmpty())
                service.getCities().appendCity(input.getText().toString());
//            service.setCities(cities);
//            adapter.notifyDataSetChanged();
        });
        builder.show();
    }

    private void clearList() {
        service.getCities().clear();
        adapter.notifyDataSetChanged();
    }

    private void saveSharedPreference() {
        sharedPreferences = getPreferences(MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        //здесь сохранение списка выбранных городов
        editor.putString(LAST_SELECT_CITY, selectCity);

        Log.i("MSG", "saveSharedPreference city = " + selectCity);

        editor.apply();
    }

    private void loadSharedPreference(CityData cities) {
        //здесь загрузка списка городов
//        cities = new CityData(getApplicationContext(), "Moscow", "Murmansk", "Yekaterinburg");
//        cities = new CityData(getApplicationContext());

        if (cities == null) return;

        sharedPreferences = getPreferences(MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences != null) {
            selectCity = (String) sharedPreferences.getAll().get(LAST_SELECT_CITY);
            Log.i("MSG", sharedPreferences.getAll().toString());
            Log.i("MSG", "loadSharedPreference city = " + selectCity);
            if (selectCity != null) {
                if (cities.getIndex(selectCity) != -1)
                    showCityInfoActivity(cities.getIndex(selectCity));
            }
        }
    }

    private void showCityInfoActivity(Integer cityId) {
        if (adapter.getCities().getCity(cityId) == null) return;
        selectCity = adapter.getCities().getCity(cityId).getName();

        Intent intentWidget = new Intent(MainActivity.BROADCAST_WIDGET);
        intentWidget.putExtra("city_name", adapter.getCities().getCity(cityId).getName());
        intentWidget.putExtra("city_temperature", adapter.getCities().getCity(cityId).getTemperature());
        sendBroadcast(intentWidget);

        Intent intent = CityInfoActivity.newIntent(this, adapter.getCities().getCity(cityId));
        startActivity(intent);
    }

    public class myAdapter extends RecyclerView.Adapter<MyViewHolder> {

        private CityData cities;

        public myAdapter() {
            cities = new CityData(getApplicationContext(), null);
        }

        public myAdapter(CityData cities) {
            this.cities = cities;
        }

        public void setData(CityData cities) {
            this.cities = cities;
        }

        public CityData getCities() {
            return cities;
        }

        public void setCities(CityData cities) {
            this.cities = cities;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
            return new MyViewHolder(inflater.inflate(R.layout.activity_city_item, parent, false));
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.bind(cities.getCity(position).getName(),
                    cities.getCity(position).getTime(),
                    cities.getCity(position).getTemperature());
        }

        @Override
        public int getItemCount() {
            return cities == null ? 0 : cities.getSize();
        }
    }


    private class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        private final MenuItem.OnMenuItemClickListener onEditMenu = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_delete:
                        service.getCities().remove(getAdapterPosition());
                        break;
                    default:
                        return false;
                }
                adapter.notifyDataSetChanged();
                return true;
            }
        };
        private TextView time;
        private TextView name;
        private TextView temperature;

        MyViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);

            this.time = itemView.findViewById(R.id.city_time);
            this.name = itemView.findViewById(R.id.city);
            this.temperature = itemView.findViewById(R.id.city_temperature);

            v.setOnCreateContextMenuListener(this);
        }

        void bind(String name, String time, String temperature) {
            if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                this.time.setText(time);
                this.name.setText(name);
                this.temperature.setText(temperature);
            }
        }

        @Override
        public void onClick(View view) {
            showCityInfoActivity(getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.context_menu, contextMenu);
            contextMenu.findItem(R.id.menu_delete).setOnMenuItemClickListener(onEditMenu);
        }
    }
}
