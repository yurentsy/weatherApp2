package com.yurentsy.weatherapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Brony on 26.03.2018.
 */

public class CityInfoActivity extends Activity {

    public static final String INTENT_CITY_ID = "city";
    public static final String INTENT_CITY_TEMP = "temp";
    public static final String INTENT_CITY_STATE = "state";

    private TextView name;
    private TextView state;
    private TextView temperature;
    private SharedPreferences sharedPreferences;
    private FloatingActionButton fab;

    public static Intent newIntent(Context context, City city) {
        Intent intent = new Intent(context, CityInfoActivity.class);
        intent.putExtra(INTENT_CITY_ID, city.getName());
        intent.putExtra(INTENT_CITY_TEMP, city.getTemperature());
        intent.putExtra(INTENT_CITY_STATE, city.getState());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_info);

        sharedPreferences = getSharedPreferences("main", MODE_PRIVATE);

        name = findViewById(R.id.city);
        state = findViewById(R.id.state);
        temperature = findViewById(R.id.temperature);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            name.setText(bundle.getString(INTENT_CITY_ID));
            state.setText(bundle.getString(INTENT_CITY_TEMP));
            temperature.setText(bundle.getString(INTENT_CITY_STATE));
        }

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.fab) {
                    StringBuilder messageText = new StringBuilder("In ");
                    messageText
                            .append(name.getText().toString()).append(" ")
                            .append(state.getText().toString()).append(" ")
                            .append(temperature.getText().toString()).append(" now");
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, messageText.toString());
                    String chooserTitle = getString(R.string.chooser_title);
                    Intent chosenIntent = Intent.createChooser(intent, chooserTitle);
                    startActivity(chosenIntent);
                }

            }
        });
    }
}
