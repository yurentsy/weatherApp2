package com.yurentsy.weatherapp;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Brony on 09.03.2018.
 */

class CityData {

    private final Context context;
    public Intent lastSelect;
    private List<String> citiesNames = new LinkedList<>();
    private List<City> citiesObj = new LinkedList<>();
    private DatabaseHelper dbHelper;
    private SQLiteDatabase database;
    private String[] citiesAllColumn = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.COLUMN_NAME,
            DatabaseHelper.COLUMN_TEMPERATURE,
            DatabaseHelper.COLUMN_TIME,
            DatabaseHelper.COLUMN_STATE
    };
    private List<City> allCitiesObjects;

    public CityData(Context context, Object nul) {
        this.context = context;
        dbHelper = new DatabaseHelper(context);
        open();
        for (String city : getAllCities()) {
            appendCity(city);
        }
        close();
    }

    public CityData(Context context) {
        this.context = context;
        dbHelper = new DatabaseHelper(context);
        open();

        for (String city : getAllCities()) {
            appendCity(city);
        }
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        database.close();
        dbHelper.close();
    }

    public City getCity(String name) {
        return citiesObj.get(citiesNames.indexOf(name));
    }

    public City getCity(Integer index) {
        return citiesObj.size() > index ? citiesObj.get(index) : null;
    }

    public Integer getIndex(String name) {
        return citiesNames.indexOf(name);
    }

    public void appendCity(String name) {
        if (name == null) return;
        if (citiesNames.contains(name))
            return;

        City city = new City(context, name);
        citiesNames.add(name);
        citiesObj.add(addCity(city));
    }

    public void remove(int pos) {
        deleteCity(citiesNames.get(pos));
        citiesNames.remove(pos);
        citiesObj.remove(pos);
    }

    public void clear() {
        citiesNames.clear();
        citiesObj.clear();
        deleteAll();
    }

    public int getSize() {
        return citiesNames.size();
    }


    public City addCity(String name, String temperature, String time, String state) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_NAME, name);
        values.put(DatabaseHelper.COLUMN_TEMPERATURE, temperature);
        values.put(DatabaseHelper.COLUMN_TIME, time);
        values.put(DatabaseHelper.COLUMN_STATE, state);
        long insertId = database.insert(DatabaseHelper.TABLE_CITIES, null, values);
        City city = new City(insertId, name, temperature, time, state);
        return city;
    }

    public City addCity(City city) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_NAME, city.getName());
        values.put(DatabaseHelper.COLUMN_TEMPERATURE, city.getTemperature());
        values.put(DatabaseHelper.COLUMN_TIME, city.getTime());
        values.put(DatabaseHelper.COLUMN_STATE, city.getState());
        long insertId = database.insert(DatabaseHelper.TABLE_CITIES, null, values);
        city.setId(insertId);
        return city;
    }

    public void editCity(long id, String name, String temperature, String time, String state) {
        ContentValues editedCity = new ContentValues();
        editedCity.put(dbHelper.COLUMN_ID, id);
        editedCity.put(dbHelper.COLUMN_NAME, name);
        editedCity.put(DatabaseHelper.COLUMN_TEMPERATURE, temperature);
        editedCity.put(DatabaseHelper.COLUMN_TIME, time);
        editedCity.put(DatabaseHelper.COLUMN_STATE, state);
        database.update(dbHelper.TABLE_CITIES, editedCity, dbHelper.COLUMN_ID + "=" + id, null);
    }

    public void deleteCity(City city) {
        String name = city.getName();
        database.delete(DatabaseHelper.TABLE_CITIES, DatabaseHelper.COLUMN_NAME + " like \"" + name + "\"", null);
    }

    public void deleteCity(String name) {
        database.delete(DatabaseHelper.TABLE_CITIES, DatabaseHelper.COLUMN_NAME + " like \"" + name + "\"", null);
    }

    public void deleteAll() {
        database.delete(DatabaseHelper.TABLE_CITIES, null, null);
    }

    public List<String> getAllCities() {
        List<String> cities = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_CITIES,
                citiesAllColumn, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            cities.add(cursor.getString(1));
//            City city = cursorToNote(cursor);
//            cities.add(city);
            cursor.moveToNext();
        }
        cursor.close();
        return cities;
    }

    public void getAllCitiesFull() {
        Cursor cursor = database.query(DatabaseHelper.TABLE_CITIES,
                citiesAllColumn, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            citiesNames.add(cursor.getString(1));
            City city = cursorToNote(cursor);
            citiesObj.add(city);
            cursor.moveToNext();
        }
        cursor.close();
    }

    private City cursorToNote(Cursor cursor) {
        return new City(
                cursor.getLong(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4)
        );
    }

    public List<City> getAllCitiesObjects() {
        return citiesObj;
    }
}
