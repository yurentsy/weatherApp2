package com.yurentsy.weatherapp;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;

/**
 * Implementation of App Widget functionality.
 */
public class WeatherWidget extends AppWidgetProvider {

    public static final String UPDATE_WIDGET_ACTION = "com.yurentsy.weatherapp.appwidget.action.APPWIDGET_UPDATE";
    private String cityName;
    private String cityTemperature;

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                         int appWidgetId) {

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.weather_widget);
        setView(views, context, appWidgetId);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    void setView(RemoteViews views, Context context, int appWidgetId) {
        views.setTextViewText(R.id.city_name, cityName);
        views.setTextViewText(R.id.city_temperature, cityTemperature);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        if (intent.getAction().equalsIgnoreCase(MainActivity.BROADCAST_WIDGET)) {
            Bundle bundle = intent.getExtras();
            cityName = bundle.getString("city_name");
            cityTemperature = bundle.getString("city_temperature");
            int appWidgetIds[] = mgr.getAppWidgetIds(new ComponentName(context, WeatherWidget.class));
            onUpdate(context, mgr, appWidgetIds);
            mgr.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.city_name);
            mgr.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.city_temperature);
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

