package com.yurentsy.weatherapp;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Brony on 03.04.2018.
 */

public class City implements Serializable {

    private Long id;
    private String name;
    private String temperature;
    private String time;
    private String state;


    public City(Long id, String name, String temperature, String time, String state) {
        this.id = id;
        this.name = name;
        this.temperature = temperature;
        this.time = time;
        this.state = state;
    }

    public City(Context context, String name) {
        Thread th = new Thread(() -> {
            JSONObject obj = WeatherDataLoader.getJSONData(context, name);
            if (obj == null) return;
            setFromJson(obj);
        });
        th.start();
        try {
            th.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setFromJson(JSONObject json) {
        final Weather weather = new Gson().fromJson(json.toString(), Weather.class);
        initWeather(weather);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getTime() {
        return time;
    }

    public String getState() {
        return state;
    }

    private void initWeather(Weather weather) {
        name = weather.getCity();
        temperature = weather.getTemp();
        time = "0:00";
        state = weather.getDescription();
    }
}
