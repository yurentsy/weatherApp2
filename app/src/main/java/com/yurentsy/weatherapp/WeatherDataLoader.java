package com.yurentsy.weatherapp;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Brony on 26.03.2018.
 */

public class WeatherDataLoader {
    private static final String OPEN_WEATHER_MAP_API = "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s";//&units=metric
    private static final String NEW_LINE = "\n";
    private static final String RESPONSE = "cod";
    private static final int ERROR_FROM_SERVER = 200;

    static JSONObject getJSONData(Context context, String city) {
        try {
            String appid = context.getString(R.string.open_weather_maps_app_id);

            URL url = new URL(String.format(OPEN_WEATHER_MAP_API, city, appid));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder rawData = new StringBuilder(1024);
            String tempVariable;
            while ((tempVariable = reader.readLine()) != null) {
                rawData.append(tempVariable).append(NEW_LINE);
            }
            reader.close();

            JSONObject jsonObject = new JSONObject(rawData.toString());
            if (jsonObject.getInt(RESPONSE) == ERROR_FROM_SERVER) {
                return jsonObject;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
