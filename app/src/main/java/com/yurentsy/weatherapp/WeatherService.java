package com.yurentsy.weatherapp;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Brony on 10.04.2018.
 */

public class WeatherService extends Service {

    public static WeatherService sInstance;
    Timer timer;
    TimerTask tTask;
    ExampleBinder binder = new ExampleBinder();
    private long interval = 1000;
    private CityData cities;
    private Intent intent;
    private String lastSelect;

    public static WeatherService getInstance() {
        return sInstance;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
//        Toast.makeText(this, "onBind", Toast.LENGTH_SHORT).show();
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
//        Toast.makeText(this, "onUnbind", Toast.LENGTH_SHORT).show();
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        timer = new Timer();
        intent = new Intent(MainActivity.BROADCAST_ACTION);
        sInstance = this;
        schedule();
    }

    void schedule() {
        if (tTask != null) tTask.cancel();
        if (interval > 0) {
            tTask = new TimerTask() {
                public void run() {
                    cities = new CityData(getApplicationContext());
                    Log.d("Service", " update");
                    intent.putExtra(MainActivity.PARAM_STATUS, MainActivity.STATUS_FINISH);
                    sendBroadcast(intent);
                }
            };
            timer.schedule(tTask, 3000, interval);
        }
    }

    @Override
    public void onDestroy() {
        cities.close();
        super.onDestroy();
    }


    public CityData getCities() {
        return cities;
    }

    public void setCities(CityData cities) {
        this.cities = cities;
    }

    public void setLastSelect(String lastSelect) {
        this.lastSelect = lastSelect;
    }

    class ExampleBinder extends Binder {
        WeatherService getService() {
            return WeatherService.this;
        }
    }
}
